#Elimina colonne uguali

def getDuplicateColumns(df):
    '''
    Resituisce la lista delle colonne con duplicati
    It will iterate over all the columns in dataframe and find the columns whose contents are duplicate.
    :param df: Dataframe object
    :return: List of columns whose contents are duplicates.
    '''
    duplicateColumnNames = set()
    # Iterate over all the columns in dataframe
    for x in range(df.shape[1]):
        # Select column at xth index.
        col = df.iloc[:, x]
        # Iterate over all the columns in DataFrame from (x+1)th index till end
        for y in range(x + 1, df.shape[1]):
            # Select column at yth index.
            otherCol = df.iloc[:, y]
            # Check if two columns at x 7 y index are equal
            if col.equals(otherCol):
                duplicateColumnNames.add(df.columns.values[y])
 
    return list(duplicateColumnNames)
df=df.drop(columns=getDuplicateColumns(df))



#correlation matrix e grafico del calore
corrmat = DataFrameTotale.corr()
k = 10 #number of variables for heatmap
cols = corrmat.nlargest(k, 'Density')['Density'].index
cm = np.corrcoef(DataFrameTotale[cols].values.T)
sns.set(font_scale=1.25)
plt.figure(figsize = (20,10))
#hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()


#grafici completi
#scatterplot
sns.set(style="ticks")
plt.rc('legend',fontsize='xx-large') 
cols = ['CC_SAE', 'CC_GLN','Density']
sns.pairplot(finalDf[cols], height = 5, hue="Density")
plt.show();



#Unire le righe con un campo corrispondente
DataFrameTotale = None
views_to_consider = ['CC', 'MLO', 'ML']
columns_to_rename = [col for col in DataFrameRadiologie.columns if col not in ['Case_ID', 'View', 'Density']]

for view in views_to_consider:
    curr_df = DataFrameRadiologie[DataFrameRadiologie['View']==view]
    curr_df = curr_df.drop(columns=['View'])

    columns_mapping = {}
    for col in columns_to_rename:
        columns_mapping[col] = view + '_' + col
    curr_df = curr_df.rename(columns=columns_mapping)

    if DataFrameTotale is None:
        DataFrameTotale = curr_df.copy()
    else:
        DataFrameTotale = DataFrameTotale.merge(curr_df, on=['Case_ID', 'Density'])


#dividere in train e validate
train, validate, test = np.split(finalDf.sample(frac=1), [int(.6*len(finalDf)), int(.8*len(finalDf))])


#best model random forest
bestmodel = None
bestaccuracy = 0
for n in [10,100,1000]:
    random_forest = RandomForestClassifier(n_estimators=n)
    random_forest.fit(X_train,Y_train)
    
    if random_forest.score(X_validate,Y_validate) > bestaccuracy:
        bestaccuracy = random_forest.score(X_validate, Y_validate)
        bestmodel = random_forest
 
