#valutation for KNN
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
Ks = 12
mean_acc = np.zeros((Ks-1))
std_acc = np.zeros((Ks-1))
ConfustionMx = [];
for n in range(1,Ks):
    
    #Train Model and Predict  
    neigh = KNeighborsClassifier(n_neighbors = n).fit(X_train,y_train)
    yhat=neigh.predict(X_test)
    mean_acc[n-1] = accuracy_score(y_test, yhat)

    
    std_acc[n-1]=np.std(yhat==y_test)/np.sqrt(yhat.shape[0])

mean_acc

plt.plot(range(1,Ks),mean_acc,'g')
plt.fill_between(range(1,Ks),mean_acc - 1 * std_acc,mean_acc + 1 * std_acc, alpha=0.10)
plt.legend(('Accuracy ', '+/- 3xstd'))
plt.ylabel('Accuracy ')
plt.xlabel('Number of Nabors (K)')
plt.tight_layout()
plt.show()

print( "The best accuracy was", mean_acc.max(), "with k=", mean_acc.argmax()+1) 
knn_classifier = KNeighborsClassifier(n_neighbors = mean_acc.argmax()+1).fit(X_train,y_train)


#valutation for Decision Tree

#!conda install -c conda-forge pydotplus -y
#!conda install -c conda-forge python-graphviz -y
from sklearn.externals.six import StringIO
import pydotplus
import matplotlib.image as mpimg
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_curve, auc
LoanTree = DecisionTreeClassifier(criterion="entropy", max_depth = 6)
LoanTree # it shows the default parameters
LoanTree.fit(X_train,y_train)
predictionTree = LoanTree.predict(X_test)
#print (predictionTree)
#print (y_test)
print("Decision Trees's Accuracy: ", accuracy_score(y_test, predictionTree))

#false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, predictionTree)
#roc_auc = auc(false_positive_rate, true_positive_rate)
#print("Decision Trees's area under the ROC: ", roc_auc)
max_depths = np.linspace(1, 20, 20, endpoint=True)
train_results = []
test_results = []

for max_depth in max_depths:
    LoanTree = DecisionTreeClassifier(criterion="entropy", max_depth=max_depth)
    LoanTree.fit(X_train, y_train)
    train_pred = LoanTree.predict(X_train)
    acc_score = accuracy_score(y_train, train_pred)
    
    # Add accuracy score to previous train results
    train_results.append(acc_score)
    
    # validation set
    y_pred = LoanTree.predict(X_test)
    acc_score = accuracy_score(y_test, y_pred)
    # Add accuracy score to previous test results
    test_results.append(acc_score)
    from matplotlib.legend_handler import HandlerLine2D

line1, = plt.plot(max_depths, train_results, 'b', label='Train Accuracy Score')
line2, = plt.plot(max_depths, test_results, 'r', label= 'Test Accuracy Score')
plt.legend(handler_map={line1: HandlerLine2D(numpoints=2)})
plt.ylabel('Accuracy score')
plt.xlabel('Tree depth')
plt.xticks(np.arange(0, 21, step=1))
plt.show()

dot_data = StringIO()
filename = "Loan_stats.png"
featureNames = Feature.columns.tolist()
targetNames = df['loan_status'].unique().tolist()

#print(featureNames)
#print(targetNames)

out=tree.export_graphviz(LoanTree,feature_names=featureNames, out_file=dot_data, class_names= np.unique(y_train), filled=True,  special_characters=True,rotate=False)  
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
graph.write_png(filename)
img = mpimg.imread(filename)
plt.figure(figsize=(100, 200))
plt.imshow(img,interpolation='nearest')


#valutition for Support Vector Machine
from sklearn import svm
#from sklearn.utils.fixes import loguniform
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    
    param_grid = [{'C': [1, 10, 100, 1000], 'kernel': ['linear']},
              {'C': [1, 10, 100, 1000],
              'gamma': [1e-4, 1e-3],
              'kernel': ['rbf'],
              'class_weight':['balanced', None]}]
              
          clf = GridSearchCV(svm.SVC(), param_grid, scoring='accuracy')
clf.fit(X_train, y_train)

print("Best parameters set found on development set:")
print()
print(clf.best_params_)
best_params = clf.best_params_
print()
print("Grid scores on development set:")
print()
means = clf.cv_results_['mean_test_score']
stds = clf.cv_results_['std_test_score']

for mean, std, params in zip(means, stds, clf.cv_results_['params']):
    print("%0.3f (+/-%0.03f) for %r"
          % (mean, std * 2, params))
    print()

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()

    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()
    
    clf = svm.SVC(C=1, gamma=0.0001, class_weight=None, kernel='rbf')
clf.fit(X_train, y_train) 
y_pred = clf.predict(X_test)
cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

print (classification_report(y_test, y_pred))

print("Best parameters set found on development set:")
print()
print(best_params)
print()
print("Accuracy score of SVM is: ", accuracy_score(y_test, y_pred))
  
  
#valutation for logistic regression

from sklearn.linear_model import LogisticRegression

LR_param_grid = {'solver':['lbfgs', 'liblinear'], 'C': [0.001, 0.01, 0.05, 0.1, 0.2, 0.5, 1]}


LR = GridSearchCV(LogisticRegression(), LR_param_grid, scoring='accuracy')
LR.fit(X_train, y_train)

print("Best parameters set found on development set:")
print()
print(LR.best_params_)
print()
print()
LR_best_params = LR.best_params_

means = LR.cv_results_['mean_test_score']
stds = LR.cv_results_['std_test_score']

for mean, std, params in zip(means, stds, LR.cv_results_['params']):
    print("%0.3f (+/-%0.03f) for %r"
          % (mean, std * 2, params))
    print()

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()

    y_true, y_pred = y_test, LR.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()
    
    #LR_best_params
LR = LogisticRegression(C=0.05, solver='lbfgs').fit(X_train,y_train)

#yhat = LR.predict(X_test)
print("Logistic Regression best accuracy score is ", accuracy_score(y_test, LR.predict(X_test))," with C = ", LR_best_params)




