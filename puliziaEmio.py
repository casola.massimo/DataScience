#!/usr/bin/env python
# coding: utf-8

# In[274]:

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from numpy import around
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from scipy import stats
import warnings
warnings.filterwarnings('ignore')
get_ipython().run_line_magic('matplotlib', 'inline')


# In[275]:


#bring the train
df_train = pd.read_csv('./input/train.csv')
y=df_train['SalePrice']
df_raw = df_train.copy()  # Save original data set, just in case.


# In[276]:


#check the decoration
df_train.info()


# In[277]:


#descriptive statistics summary
df_train['SalePrice'].describe()


# In[278]:


#histogram
sns.distplot(df_train['SalePrice']);
#Quindi realizzo che non ci sono valori negativi e che i valori ha un netto picco.


# Coerentemente con il grafico la media sarà più piccola della moda( e di conseguenza della mediana); Ci sarà un asimmetria positiva.
# 
# Moda < Mediana < Media; la coda è a destra.
# 
# Moda > Mediana > Media; la coda è a sinistra.
# 
# Moda = Mediana = Media; campana.
# 
# Skewness è la misura dell'asimmetria;

# In[279]:


print("Il prezzo medio delle case è",df_train.SalePrice.mean())


# In[280]:


df_train.loc[:,'SalePrice'].mode()


# Se il coefficiente di curtosi è:
# 
# curtosi > 0 la curva si definisce leptocurtica, cioè più "appuntita" di una normale.
# 
# curtosi < 0 la curva si definisce platicurtica, cioè più "piatta" di una normale.
# 
# curtosi = 0 la curva si definisce normocurtica (o mesocurtica), cioè "piatta" come una normale.

# In[281]:


#skewness and kurtosis

#curtosi allontanamento dalla normalità distributiva
print("Skewness: %f" % df_train['SalePrice'].skew())
print("Kurtosis: %f" % df_train['SalePrice'].kurt())


# Guardo la dispersione dei dati nel grafico.
# 
# Un grafico di dispersione è spesso usato quando una delle variabili è sotto controllo dello sperimentatore. Un parametro che è incrementato e/o decrementato sistematicamente è chiamato parametro di controllo o variabile indipendente, ed è arbitrariamente posto sull'asse orizzontale. La variabile misurata (o dipendente) è arbitrariamente posta sull'asse verticale. Se non esistono variabili dipendenti, ogni variabile può essere messa su un asse a piacere. Il grafico di dispersione può' essere utile per visualizzare il grado di correlazione (cioè di dipendenza lineare) tra le due variabili. Un grafico a dispersione può suggerire vari tipi di correlazione tra variabili con un certo intervallo di confidenza. Le correlazioni possono essere positive, negative o nulle.

# Dice che per lui gli elementi principali sono questi:
# 
# OverallQual  -> Intero
# 
# YearBuilt  -> Intero
# 
# TotalBsmtSF  -> Intero
# 
# GrLivArea  ->Intero
# 
# Allora me li studio:

# Ora la logica usata è che essendo molto sparsi i valori di "GrLivArea" essendo aree diverse, guardiamo come il prezzo varia a seconda della dimensione. Lo stesso discorso vale per l'area sottostante del seminterrato.

# In[282]:


#scatter plot grlivarea/saleprice
#GrLivArea è l'area calpestabile
var = 'GrLivArea'
data = pd.concat([df_train['SalePrice'], df_train[var]], axis=1)
data.plot.scatter(x=var, y='SalePrice', ylim=(0,800000));


# In[283]:


#scatter plot totalbsmtsf/saleprice
#TotalBsmtSF dimensioni del seminterrato
var = 'TotalBsmtSF'
data = pd.concat([df_train['SalePrice'], df_train[var]], axis=1)
data.plot.scatter(x=var, y='SalePrice', ylim=(0,800000));


# Essendo invece un valore da 0 a 10 il livello di qualità, vediamo un diagramma di scatole a baffi per vedere come si colloca il prezzo della casa rispetto la variabile. Lo stesso discorso vale per la data di produzione della casa.

# In[284]:


#box plot overallqual/saleprice
#livello complessivo della qualità dei materiali e delle rifiniture
var = 'OverallQual'
data = pd.concat([df_train['SalePrice'], df_train[var]], axis=1)
f, ax = plt.subplots(figsize=(8, 6))
fig = sns.boxplot(x=var, y="SalePrice", data=data)
fig.axis(ymin=0, ymax=800000);


# Possiamo vedere che una casa più recente spesso è più cara.

# In[285]:


#anno di costruzione
var = 'YearBuilt'
data = pd.concat([df_train['SalePrice'], df_train[var]], axis=1)
f, ax = plt.subplots(figsize=(16, 8))
fig = sns.boxplot(x=var, y="SalePrice", data=data)
fig.axis(ymin=0, ymax=800000);
plt.xticks(rotation=90);


# Questo lo aggiungo rispetto a quello online;
# 
# Lui mi dice che dalle scatole è evidente la correlazione positiva dei 4 campi. A questo punto vedo con Pearson come va in una correlazione lineare in un heatmap.
# 
# Se si ha un valore <0,3 correlazione debole;
# 
# Se si ha un valore compreso da >03 e <0,7 correlazione moderata;
# 
# Se si ha un valore >0,7  correlazione forte.

# In[286]:


df_corr=df_train[['OverallQual','YearBuilt','TotalBsmtSF','GrLivArea','SalePrice']]
#Using Pearson Correlation
plt.figure(figsize=(15,10))
cor = df_corr.corr()
sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
plt.show()


# In[287]:


#Seleziona il valore assoluto di sales Price, nel mio caso essendo sempre positivo non serve
cor_target = abs(cor['SalePrice'])
#Seleziono solo e correlazioni superiori al 0.5
relevant_features = cor_target[cor_target>0.5]
relevant_features


# Diamo una visione generale anche se confusa

# In[288]:


#correlation matrix
corrmat = df_train.corr()
f, ax = plt.subplots(figsize=(12, 9))
sns.heatmap(corrmat, vmax=.8, square=True);


# In[289]:


#la funzione nlargest ritorna le n righe pià grandi ordinate di grandezza
#Le prime 9 più correlate al SalePrice

k = 10 #number of variables for heatmap
cols = corrmat.nlargest(k, 'SalePrice')['SalePrice'].index
cm = np.corrcoef(df_train[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()


# Quello che vediamo è che l'area del garage e il numero di macchine sono importanti però tra di loro correlati.
# Possiamo tenerne uno, in questo caso terremo garage cars essendo quello con la correlazione più alta

# Quindi prendo questi valori che sono i più correlati è inizio a studiarli

# In[290]:


#scatterplot
sns.set()
cols = ['SalePrice', 'OverallQual', 'GrLivArea', 'TotalBsmtSF', 'GarageCars', 'FullBath', 'YearBuilt']
sns.pairplot(df_train[cols], size = 2.5)
plt.show();


# Inizia il lavoro dei nulli, come utilizzarli

# In[291]:


#missing data
total = df_train.isnull().sum().sort_values(ascending=False)
percent = ((df_train.isnull().sum()/df_train.isnull().count()).sort_values(ascending=False))*100
missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
missing_data[missing_data['Percent'] >0]


# Elimineremo i valori sotto il 15. Nel nostro caso il 15% è 279 elementi.
# 
# N.B. nell'esempio che seguo lui cancella tutte le colonne.
# 
# Per i valori sopra il 15% il discorso è chiaro.
# 
# Per i garage invece sostiene che avendo già il campo di maggiore importanza rispetto i garage, cioè il campo che indica quante macchine contiene, gli altri non servono. Gli altri dati sul seminterrato vale lo stesso discorso.
# 
# Mansarda sostiene arbitrariamente che non interessano.
# 
# Electrical elimina solo la riga vuota

# In[292]:


#dealing with missing data
df_train = df_train.drop((missing_data[missing_data['Total'] > 1]).index,1)
df_train = df_train.drop(df_train.loc[df_train['Electrical'].isnull()].index)


# In[293]:


df_train.isnull().sum().max() #just ch<ecking that there's no missing data missing...


# L'idea alla base StandardScaler è che possa trasformare i dati in modo tale che la sua distribuzione avrà un valore medio 0 e deviazione standard di 1. Data la distribuzione dei dati, ciascun valore nell'insieme di dati avrà il valore della media del campione sottratto, e poi diviso dalla deviazione standard dell'intero set di dati.
# 
#     
# La riduzione di scala standardizza la serie di valori. La standardizzazione è un metodo di riduzione di scala che trasforma una serie di valori in una distribuzione normale standard con media uguale a zero e devianza standard uguale a uno.
# 
# La standardizzazione consiste nel sottrarre la media (μ) da ogni valore (x) del vettore e dividere la differenza per la devianza standard (σ).

# In[294]:


#standardizing data
sc=StandardScaler()
saleprice_scaled = sc.fit_transform(df_train['SalePrice'][:,np.newaxis])


# In[295]:


low_range = saleprice_scaled[saleprice_scaled[:,0].argsort()][:10]
high_range= saleprice_scaled[saleprice_scaled[:,0].argsort()][-10:]
print("L'intervallo esterno (basso) della distribuzione è :")
print(low_range)
print("L'intervallo esterno (alto) della distribuzione è :")
print(high_range)


# Nella gamma bassa i valori sono simili e non troppo lontano da 0.
# Gamma alta valori sono lontani dallo 0 arrivando a 7. Questi valori sono davvero fuori gamma.
# Per ora ci occuperemo di non considerare uno qualsiasi di questi valori come un valore errato ma dobbiamo essere cauti con quei due 7.

# In[296]:


#bivariate analysis saleprice/grlivarea
var = 'GrLivArea'
data = pd.concat([df_train['SalePrice'], df_train[var]], axis=1)
data.plot.scatter(x=var, y='SalePrice', ylim=(0,800000));


# Riprendendo il grafico precendete, si può vedere che c'erano dei valori che falsavano allontanandosi dalla massa.
# 
# Sopratutto i due valori grandi di GrLivArea, con prezzi così bassi, risultano chiaramente un anomalia.

# In[297]:


#deleting points, selezionando i due più grandi
df_train.sort_values(by = 'GrLivArea', ascending = False)[:2]


# In[298]:


#ora che li so li elimino
df_train = df_train.drop(df_train[df_train['Id'] == 1299].index)
df_train = df_train.drop(df_train[df_train['Id'] == 524].index)


# In[299]:


#deleting points, selezionando i due più grandi
df_train.sort_values(by = 'SalePrice', ascending = False)[:2]


# In[300]:


#Lui non lo fa , ma elimino anche i 2 che vanno molto fuori per prezzo alto
df_train = df_train.drop(df_train[df_train['Id'] == 692].index)
df_train = df_train.drop(df_train[df_train['Id'] == 1183].index)


# In[301]:


#bivariate analysis saleprice/grlivarea
var = 'TotalBsmtSF'
data = pd.concat([df_train['SalePrice'], df_train[var]], axis=1)
data.plot.scatter(x=var, y='SalePrice', ylim=(0,800000));


# Ora è il momento di realizzare bene chi è SalesPrice.Applicheremo le tecniche di statistica multivariata.
# 

#     NORMALE
# 
# In teoria della probabilità e statistica, la distribuzione normale multivariata o distribuzione gaussiana multivariata o vettore gaussiano è una generalizzazione della distribuzione normale a dimensioni più elevate. Un vettore di variabili aleatorie ha una distribuzione normale multivariata se ogni combinazione lineare delle sue componenti ha distribuzione normale.

#     Omoschedasticità
# 
# Nella statistica, l'omoschedasticità è una proprietà che possiede una collezione di variabili aleatorie nel momento in cui hanno tutte la stessa varianza. In questo caso le variabili si dicono omoschedastiche, altrimenti vengono definite eteroschedastiche. 

#     Lineare
# 
# Il modo più comune per valutare la linearità è di esaminare grafici a dispersione e ricercando modelli lineari. Se i modelli non sono lineari, varrebbe la pena tentarne una trasformazione. Tuttavia, non arriveremo a questo perché la maggior parte dei grafici a dispersione che abbiamo visto sembra avere relazioni lineari.

#     Assenza di errori correlati 
# 
# Errori correlati, come la definizione suggerisce, accadono quando un errore è correlato a un altro. Per esempio, se un errore positivo commette un errore negativo sistematicamente, significa che c'è una relazione tra queste variabili.Non è il nostro caso.Tuttavia, se si rileva qualcosa, si può provare ad aggiungere una variabile che può spiegare l'effetto che si sta ricevendo. Questa è la soluzione più comune per gli errori correlati.

# Sempre di SalesPrice:
# 
# Istogramma - curtosi e asimmetria.
# 
# Normale probabilità plot - La fistribuzione dei dati dovrebbe seguire da vicino la diagonale che rappresenta la distribuzione normale.

# In[302]:


#histogram and normal probability plot
sns.distplot(df_train['SalePrice'], fit=norm);


# In[303]:


fig = plt.figure()
res = stats.probplot(df_train['SalePrice'], plot=plt)


# Ok, SalePrice non è una distribuzione normale, ha un' asimmetria positiva e non segue la diagonale delle probabilità (da approfondire).
# Non tutto è perduto, possiamo risolvere il problema.
# 
# In caso di asimmetria positiva, spesso, la trasformazione logaritmica funziona bene.

# In[304]:


#applying log transformation
df_train['SalePrice'] = np.log(df_train['SalePrice'])


# In[305]:


#transformed histogram and normal probability plot
sns.distplot(df_train['SalePrice'], fit=norm);
fig = plt.figure()
res = stats.probplot(df_train['SalePrice'], plot=plt)


# Ora i risultati sono molto buoni. Vediamo cosa accade con i parametri GrLivArea,TotalBsmtSF

# In[306]:


#data transformation
df_train['GrLivArea'] = np.log(df_train['GrLivArea'])


# In[307]:


#transformed histogram and normal probability plot
sns.distplot(df_train['GrLivArea'], fit=norm);
fig = plt.figure()
res = stats.probplot(df_train['GrLivArea'], plot=plt)


# In[308]:


#histogram and normal probability plot
sns.distplot(df_train['TotalBsmtSF'], fit=norm);
fig = plt.figure()
res = stats.probplot(df_train['TotalBsmtSF'], plot=plt)


# Qui però nasce il problema che molti valori sono uguali a 0, cioè non c'è un seminterrato. Faremo quindi una variabile che dice se c'è o non c'è il seminterrato.

# In[309]:


#creaeremo una colonna per la nuova variabile (essendo biniario ne basta una)
#se l'area >0 1 altrimenti 0
#prima riempio tutto di 0, poi metto gli 1
df_train['HasBsmt'] = pd.Series(len(df_train['TotalBsmtSF']), index=df_train.index)
df_train['HasBsmt'] = 0 
df_train.loc[df_train['TotalBsmtSF']>0,'HasBsmt'] = 1


# In[310]:


#transform data
df_train.loc[df_train['HasBsmt']==1,'TotalBsmtSF'] = np.log(df_train['TotalBsmtSF'])


# In[311]:


#histogram and normal probability plot
sns.distplot(df_train[df_train['TotalBsmtSF']>0]['TotalBsmtSF'], fit=norm);
fig = plt.figure()
res = stats.probplot(df_train[df_train['TotalBsmtSF']>0]['TotalBsmtSF'], plot=plt)


# A questo punto verifichiamo la dispersione rispetto dimensione e prezzo

# In[312]:


#scatter plot
plt.scatter(df_train['GrLivArea'], df_train['SalePrice']);
#Più o meno ci siamo


# In[313]:


#get_dummies trasporta le righe in colonne
dfHouse = pd.get_dummies(df_train)


# In[354]:


# machine learning
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.metrics import accuracy_score


# I dati sono 1455 li divido 80% e 20%.

# In[334]:


X_train=dfHouse[0:1119]
X_test=dfHouse[1120:1454]


# In[335]:


Y_train=X_train['SalePrice']
Y_test=X_test['SalePrice']


# In[336]:


X_train=X_train.drop(['SalePrice'],axis=1)
X_test=X_test.drop(['SalePrice'],axis=1)


# A questo punto ho i valori divisi tra caratteristiche e target.

# In[349]:


ll = LinearRegression()
ll.fit(X_train, Y_train)
Y_pred_train = ll.predict(X_train)


# In[350]:


print("MSE train: %f" % mean_squared_error(Y_train,Y_pred_train))
print("R2 train: %f" % r2_score(Y_train, Y_pred_train))


# In[351]:


Y_pred_test = ll.predict(X_test)


# In[352]:


print("MSE test: %f" % mean_squared_error(Y_test,Y_pred_test))
print("R2 test: %f" % r2_score(Y_test, Y_pred_test))


# In[361]:


confrontTrain = pd.DataFrame({'Actual':Y_test, 'Predicted': Y_pred_test,'Value' : Y_test-Y_pred_test})
confrontTrain.sort_values(by='Value',ascending=False)[:10]

